import { RouterState, connectRouter, routerMiddleware, CallHistoryMethodAction } from 'connected-react-router';
import thunk from 'redux-thunk';
import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { createBrowserHistory } from 'history';
import logger from 'redux-logger';

import { AuthState, AuthActions, authReducer } from './auth';

export const history = createBrowserHistory();

export interface RootState {
  auth: AuthState;
  router: RouterState;
}

export type RootAction = AuthActions | CallHistoryMethodAction;

const rootReducer = combineReducers<RootState>({
  auth: authReducer,
  router: connectRouter(history),
});

declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// step 5: createStore
export default createStore<RootState, RootAction, {}, {}>(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk), applyMiddleware(logger), applyMiddleware(routerMiddleware(history)))
);
