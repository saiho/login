import { push, CallHistoryMethodAction } from "connected-react-router";
import { Dispatch } from "redux";

export interface AuthState {
  isAuthenticated: boolean | null;
  errorMessage: string
}

const initialState = {
  isAuthenticated: null,
  errorMessage: ""
}

export const LOGIN_SUCCESSED = '@@auth/LOGIN_SUCCESSED';
export const LOGIN_FALIED = '@@auth/LOGIN_FALIED';
export const LOGOUT_SUCCESSED = '@@auth/LOGOUT_SUCCESSED';

interface LoginSccessed {
  type: typeof LOGIN_SUCCESSED;
}

interface LoginFailed {
  type: typeof LOGIN_FALIED;
  errorMessage: string;
}

interface LogoutSccessed {
  type: typeof LOGOUT_SUCCESSED;
}

export const loginSccessed = (): LoginSccessed => ({
  type: LOGIN_SUCCESSED,
});

export const loginFailed = (errorMessage: string): LoginFailed => ({
  type: LOGIN_FALIED,
  errorMessage,
});

export const logoutSccessed = (): LogoutSccessed => ({
  type: LOGOUT_SUCCESSED,
});

export type AuthActions = LoginSccessed | LoginFailed | LogoutSccessed;

export function authReducer(state: AuthState = initialState, action: AuthActions) {
  switch (action.type) {
    case LOGIN_SUCCESSED:
      return {
        ...state,
        isAuthenticated: true,
        msg: ""
      }
    case LOGIN_FALIED:
      return {
        ...state,
        msg: action.errorMessage
      }
    case LOGOUT_SUCCESSED:
      return {
        ...state,
        isAuthenticated: false,
        msg: ""
      }
    default:
      return state;
  }
}

export function loginThunk(email: string, password: string) {
  return async (dispatch: Dispatch<AuthActions | CallHistoryMethodAction>) => {
    const loginResponse = await fetch(`${process.env.REACT_APP_API_SERVER}/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ email, password })
    });

    const loginResult = await loginResponse.json();

    if (loginResponse.status !== 200) {
      dispatch(loginFailed(loginResult.message));
    } else {
      localStorage.setItem('token', loginResult.token);
      dispatch(loginSccessed());
      dispatch(push('/'));
    }
  }
}

export function logoutThunk() {
  return async (dispatch: Dispatch<AuthActions | CallHistoryMethodAction>) => {
    dispatch(logoutSccessed());
    localStorage.removeItem('token');
    dispatch(push('/'));
  }
}

export const restoreLoginThunk = () => {
  return async (dispatch: Dispatch<AuthActions | CallHistoryMethodAction>) => {
    const token = localStorage.getItem('token');
    if (token) {
      const res = await fetch(`${process.env.REACT_APP_API_SERVER}/user/info`, {
        headers: {
          Authorization: `Bearer ${token}`
        },
      });
      if (res.status === 200) {
        dispatch(loginSccessed());
        dispatch(push('/'));
      } else {
        dispatch(loginFailed(''));
      }
    } else {
      dispatch(loginFailed(''));
    }
  }
}

export function loginFacebook(accessToken: string) {
  return async (dispatch: Dispatch<AuthActions | CallHistoryMethodAction>) => {
    const res = await fetch(`${process.env.REACT_APP_API_SERVER}/login/facebook`, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify({ accessToken })
    })
    const result = await res.json();

    if (res.status !== 200) {
      dispatch(loginFailed(result.message));
    } else {
      localStorage.setItem('token', result.token);
      dispatch(loginSccessed())
      dispatch(push('/'));
    }
  }
}