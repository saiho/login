import React, { useEffect } from "react";
import "./App.css";
import HomePage from "./Pages/HomePage";
import LandingPage from "./Pages/LandingPage";
import { RootState } from "./store/store";
import { useDispatch, useSelector } from "react-redux";
import { Route, Switch } from "react-router";
import { PrivateRoute } from "./Components/PrivateRoute";
import { restoreLoginThunk } from "./store/auth";
import ResetPasswordPage from "./Pages/ResetPasswordPage";

function App() {
  const dispatch = useDispatch();
  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated
  );

  useEffect(() => {
    if (isAuthenticated == null) {
      dispatch(restoreLoginThunk());
    }
  }, [dispatch, isAuthenticated]);

  return (
    <div className="App">
      <Switch>
        <Route path="/landing" exact component={LandingPage} />
        <Route
          path="/reset_password/:userId/:token"
          component={ResetPasswordPage}
        />
        <PrivateRoute path="/" exact component={HomePage} />
      </Switch>
    </div>
  );
}

export default App;
