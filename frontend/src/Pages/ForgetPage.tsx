import React, { useState } from "react";

export default function ForgetPage() {
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [isProcessing, setIsProcessing] = useState(false);

  const handleEmailChange = (event: any) => setEmail(event.target.value);

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    setIsProcessing(true);

    const res = await fetch(
      `${process.env.REACT_APP_API_SERVER}/password/forgot`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email }),
      }
    );
    const result = await res.json();
    if (res.status !== 200) {
      setMessage(result.message);
    } else {
      setMessage("Check your email");
      setIsProcessing(false);
    }
  };

  return (
    <form onSubmit={handleSubmit} noValidate>
      <div className="form-group">
        <label htmlFor="email-recover">
          Enter your email to recover your password
        </label>
        <input
          type="email"
          name="email-recover"
          value={email}
          onChange={handleEmailChange}
          className="form-control"
          placeholder="Enter email"
          id="email-recover"
        />
        <div className="text-danger">{message}</div>
      </div>
      <input
        type="submit"
        value="Submit"
        className="btn btn-primary"
        disabled={isProcessing}
      />
    </form>
  );
}
