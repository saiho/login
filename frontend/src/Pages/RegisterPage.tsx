import { push } from "connected-react-router";
import React, { useState } from "react";
import { useDispatch } from "react-redux";

import "../App.css";

export default function RegisterPage() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [lastNameError, setLastNameError] = useState("");
  const [firstNameError, setFirstNameError] = useState("");
  const [avatar, setAvatar] = useState<any | null>(null);

  const handleEmailChange = (event: any) => setEmail(event.target.value);
  const handlePasswordChange = (event: any) => setPassword(event.target.value);
  const handleFirstNameChange = (event: any) =>
    setFirstName(event.target.value);
  const handleLastNameChange = (event: any) => setLastName(event.target.value);
  const dispatch = useDispatch();

  const handleSubmit = async (event: any) => {
    event.preventDefault();

    if (validate()) {
      const userData = new FormData();
      userData.append("firstName", firstName);
      userData.append("lastName", lastName);
      userData.append("email", email);
      userData.append("password", password);

      if (avatar) userData.append("avatar", avatar);

      const response = await fetch(
        `${process.env.REACT_APP_API_SERVER}/register`,
        {
          method: "POST",
          body: userData,
        }
      );

      let result = await response.json();
      console.log(result);
      dispatch(push("/"));
    }
  };

  const validate = () => {
    let isValid = true;
    const emailPattern = new RegExp(
      /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
    );
    const passwordPattern = new RegExp("^(?=.{8,})(?=.*[0-9])");

    if (firstName === "") {
      isValid = false;
      setFirstNameError("Missing first Name");
    } else {
      setFirstNameError("");
    }

    if (lastName === "") {
      isValid = false;
      setLastNameError("Missing last Name");
    } else {
      setLastNameError("");
    }

    if (email === "") {
      isValid = false;
      setEmailError("Missing email");
    } else if (!emailPattern.test(email)) {
      isValid = false;
      setEmailError("Invalid email");
    } else {
      setEmailError("");
    }

    if (password === "") {
      isValid = false;
      setPasswordError("Missing password");
    } else if (!passwordPattern.test(password)) {
      isValid = false;
      setPasswordError(
        "Must be eight characters or longer & contain 1 numeric character"
      );
    } else {
      setPasswordError("");
    }

    return isValid;
  };

  return (
    <div>
      <h3>Register</h3>
      <form onSubmit={handleSubmit} noValidate>
        <div className="form-group">
          <input
            type="file"
            name="avtar"
            onChange={(e) => {
              if (e.target.files) setAvatar(e.target.files[0]);
            }}
          ></input>
        </div>
        <div className="form-group">
          <label htmlFor="firstName">First name</label>
          <input
            type="firstName"
            name="firstName"
            value={firstName}
            onChange={handleFirstNameChange}
            className="form-control"
            placeholder="Enter first name"
            id="firstName"
          />
          <div className="small text-danger">{firstNameError}</div>
        </div>
        <div className="form-group">
          <label htmlFor="lastName">Last Name</label>
          <input
            type="lastName"
            name="lastName"
            value={lastName}
            onChange={handleLastNameChange}
            className="form-control"
            placeholder="Enter last name"
            id="lastName"
          />
          <div className="small text-danger">{lastNameError}</div>
        </div>
        <div className="form-group">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            name="email"
            value={email}
            onChange={handleEmailChange}
            className="form-control"
            placeholder="Enter email"
            id="email"
          />
          <div className="small text-danger">{emailError}</div>
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            name="password"
            value={password}
            onChange={handlePasswordChange}
            className="form-control"
            placeholder="Enter password"
            id="password"
          />
          <div className=" small text-danger">{passwordError}</div>
        </div>
        <input type="submit" value="Register" className="btn btn-primary" />
      </form>
    </div>
  );
}
