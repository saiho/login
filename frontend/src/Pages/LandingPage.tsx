import React, { useState } from "react";
import ForgetPage from "./ForgetPage";
import LoginPage from "./LoginPage";
import RegisterPage from "./RegisterPage";

enum Page {
  Login,
  Register,
  Forget,
}

export default function LandingPage() {
  const [page, setPage] = useState<Page>(Page.Login);

  return (
    <div>
      <nav className="navbar">
        <div>Landing Page</div>
        <div className="navbar-buttons">
          <div onClick={() => setPage(Page.Login)}>Login</div>
          <div onClick={() => setPage(Page.Register)}>Register</div>
          <div onClick={() => setPage(Page.Forget)}>Forget Password</div>
        </div>
      </nav>
      {page === Page.Login && <LoginPage />}
      {page === Page.Register && <RegisterPage />}
      {page === Page.Forget && <ForgetPage />}
    </div>
  );
}
