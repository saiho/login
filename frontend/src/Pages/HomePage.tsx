import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { logoutThunk } from "../store/auth";
import "../App.css";

interface User {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  avatar: string;
  created_at: Date;
  updated_at: Date;
}

export default function HomePage() {
  const [user, setUser] = useState<User | null>(null);
  const [isShown, setIsShown] = useState(false);
  const [error, setError] = useState("");

  const dispatch = useDispatch();
  const handleLogout = () => {
    dispatch(logoutThunk());
  };

  const fetchUser = async () => {
    const token = localStorage.getItem("token");
    const userRes = await fetch(
      `${process.env.REACT_APP_API_SERVER}/user/info`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const result = await userRes.json();
    if (userRes.status === 200) {
      console.log(result);
      setUser(result);
    } else {
      setError(result.message);
    }
  };

  useEffect(() => {
    fetchUser();
  }, []);

  return (
    <div>
      <nav className="home-navbar">
        <div className="logout" onClick={handleLogout}>
          logout
        </div>
        <div className="profile">
          <div>{`${user?.firstName} ${user?.lastName}`}</div>
          <div
            className="avatar"
            onMouseEnter={() => setIsShown(true)}
            onMouseLeave={() => setIsShown(false)}
          >
            <img
              src={`${user?.avatar}`}
              alt="avatar"
              style={{ height: "100%" }}
            />
          </div>
        </div>
        {isShown && (
          <div className="popup">
            <h6>ID: {user?.id}</h6>
            <h6>Email: {user?.email}</h6>
          </div>
        )}
      </nav>
      <h3>HomePage</h3>
    </div>
  );
}
