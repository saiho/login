import React, { useState } from "react";
import ReactFacebookLogin, {
  ReactFacebookLoginInfo,
} from "react-facebook-login";
import { useDispatch, useSelector } from "react-redux";
import "../App.css";
import { loginFacebook, loginThunk } from "../store/auth";
import { RootState } from "../store/store";

const LoginPage = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");

  const handleEmailChange = (event: any) => setEmail(event.target.value);
  const handlePasswordChange = (event: any) => setPassword(event.target.value);

  const errorMessage = useSelector(
    (state: RootState) => state.auth.errorMessage
  );

  const dispatch = useDispatch();

  const handleSubmit = async (event: any) => {
    event.preventDefault();

    if (validate()) {
      dispatch(loginThunk(email, password));
    }
  };

  const validate = () => {
    let isValid = true;
    const emailPattern = new RegExp(
      /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
    );
    const passwordPattern = new RegExp("^(?=.{8,})(?=.*[0-9])");

    if (email === "") {
      isValid = false;
      setEmailError("Missing email");
    } else if (!emailPattern.test(email)) {
      isValid = false;
      setEmailError("Invalid email");
    } else {
      setEmailError("");
    }

    if (password === "") {
      isValid = false;
      setPasswordError("Missing password");
    } else if (!passwordPattern.test(password)) {
      isValid = false;
      setPasswordError(
        "Must be eight characters or longer & contain 1 numeric character"
      );
    } else {
      setPasswordError("");
    }

    return isValid;
  };

  const fBOnCLick = () => {
    return null;
  };

  const fBCallback = (
    userInfo: ReactFacebookLoginInfo & { accessToken: string }
  ) => {
    console.log(userInfo);
    if (userInfo.accessToken) {
      dispatch(loginFacebook(userInfo.accessToken));
    }
    return null;
  };

  return (
    <div>
      <h3>Login</h3>
      <form onSubmit={handleSubmit} noValidate>
        <div className="form-group">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            name="email"
            value={email}
            onChange={handleEmailChange}
            className="form-control"
            placeholder="Enter email"
            id="email"
          />
          <div className="text-danger">{emailError}</div>
        </div>

        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            name="password"
            value={password}
            onChange={handlePasswordChange}
            className="form-control"
            placeholder="Enter password"
            id="password"
          />
          <div className="text-danger">{passwordError}</div>
        </div>
        <input type="submit" value="Login" className="btn btn-primary" />
        <div className="text-danger">{errorMessage}</div>
        <ReactFacebookLogin
          appId={process.env.REACT_APP_FACEBOOK_APP_ID || ""}
          autoLoad={false}
          fields="name,email,picture"
          onClick={fBOnCLick}
          callback={fBCallback}
        />
      </form>
    </div>
  );
};

export default LoginPage;
