import React, { useState } from "react";
import { useParams } from "react-router-dom";

export default function ResetPasswordPage() {
  const { userId, token } = useParams<{ userId: string; token: string }>();
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");

  const handleSubmit = async (event: any) => {
    event.preventDefault();

    if (validate()) {
      const res = await fetch(
        `${process.env.REACT_APP_API_SERVER}/password/reset`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ userId, token, password }),
        }
      );
      let result = await res.json();

      setMessage(result.message);
    }
  };

  const handlePasswordChange = (event: any) => setPassword(event.target.value);

  const validate = () => {
    let isValid = true;
    const passwordPattern = new RegExp("^(?=.{8,})(?=.*[0-9])");

    if (password === "") {
      isValid = false;
      setMessage("Missing password");
    } else if (!passwordPattern.test(password)) {
      isValid = false;
      setMessage(
        "Must be eight characters or longer & contain 1 numeric character"
      );
    } else {
      setMessage("");
    }
    return isValid;
  };

  return (
    <form onSubmit={handleSubmit} noValidate>
      <div className="form-group">
        <label htmlFor="new-password">Enter your new password</label>
        <input
          type="password"
          name="new-password"
          value={password}
          onChange={handlePasswordChange}
          className="form-control"
          placeholder="Enter new Password"
          id="new-password"
        />
        <div className="text-danger">{message}</div>
      </div>
      <input type="submit" value="Submit" className="btn btn-primary" />
    </form>
  );
}
