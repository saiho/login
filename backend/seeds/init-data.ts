import * as Knex from "knex";
import { hashPassword } from '../hash';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("users").del();

    const hashedPassword = await hashPassword("123456789");

    // Inserts seed entries
    await knex("users").insert([
        { firstName: "John", lastName: "S", email: "john@hotmail.com", password: hashedPassword, avatar: `${process.env.HOST}/uploads/no-image.png` },
        { firstName: "Hugo", lastName: "Cheuk", email: "hugo@hotmail.com", password: hashedPassword, avatar: `${process.env.HOST}/uploads/no-image.png` },
    ]);
};
