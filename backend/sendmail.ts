import nodemail from 'nodemailer';
import dotenv from 'dotenv';
import Mail from 'nodemailer/lib/mailer';
dotenv.config();

function sendEmail(message: Mail.Options) {
  return new Promise((res, rej) => {
    const transporter = nodemail.createTransport({
      service: 'gmail',
      auth: {
        user: process.env.GOOGLE_USER,
        pass: process.env.GOOGLE_PASSWORD
      }
    });

    transporter.sendMail(message, (err, info) => {
      if (err) {
        rej(err)
      } else {
        res(info)
      }
    });
  })
}

export function sendResetPasswordEmail(toUser: string, hash: string) {
  console.log(process.env.GOOGLE_USER, process.env.GOOGLE_PASSWORD);
  const message = {
    from: process.env.GOOGLE_USER,
    // user.email in production
    to: process.env.GOOGLE_USER,
    subject: `Reset Password`,
    html: `
    <h3>Hi ${toUser}<h3>
    <p>To rest your password please follow this link: <a target="_" href="${process.env.DOMAIN}/reset_password/${hash}">Link</a></p>
  `
  }
  return sendEmail(message);
};