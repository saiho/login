declare global {
  namespace Express {
    interface Request {
      user?: {
        id: number;
        first: string;
        lastname: string;
        email: string;
        avatar: string;
        created_at: Date;
        updated_at: Date;
      };
    }
  }
}

export interface User {
  id: number;
  first: string;
  lastname: string;
  email: string;
  password: string;
  avtar: string;
  created_at: Date;
  updated_at: Date;
}