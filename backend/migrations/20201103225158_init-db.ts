import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("users", table => {
    table.increments();
    table.string('firstName').notNullable();
    table.string('lastName').notNullable();
    table.string('email').notNullable().unique();
    table.string('password').notNullable();
    table.string('avatar');
    table.timestamps(false, true);
  });

  await knex.schema.createTable("hashes", table => {
    table.increments();
    table.integer('user_id').notNullable();
    table.foreign('user_id').references('users.id');
  });
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("users");
}

