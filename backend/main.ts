import bodyParser from 'body-parser';
import express from 'express';
import multer from 'multer';
import cors from 'cors';
import jwtSimple from 'jwt-simple';
import jwt from './jwt';
import fetch from 'node-fetch';
import { User } from './model';
import dotenv from 'dotenv';
dotenv.config();
import { sendResetPasswordEmail } from './sendmail';

import Knex from 'knex';
import configs from './knexfile';
import { checkPassword, hashPassword } from './hash';
import { isLoggedIn } from './guards';
export const knex = Knex(configs['development']);

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.use(express.static("public"));

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `${__dirname}/public/uploads`);
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
  }
});

export const upload = multer({ storage });

app.post("/register", upload.single("avatar"), async (req, res) => {
  try {
    const fileName = req.file ? req.file.filename : `no-image.png`;
    const { firstName, lastName, email, password } = req.body;
    const hashedPassword = await hashPassword(password);
    await knex("users").insert({ firstName, lastName, email, password: hashedPassword, avatar: `${process.env.HOST}/uploads/${fileName}` });
    res.json({ message: "Registration succeeded" })
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.post('/password/forgot', async (req, res) => {
  try {
    const { email } = req.body;
    if (!email) {
      res.status(401).json({ message: "Missing Email" });
      return;
    }
    const user = await knex("users").where("email", email).first();
    if (!user) {
      res.status(401).json({ message: "Invalid email" });
      return;
    }

    const payload = {
      userId: user.id,
    };
    const secret = user.password + "-" + user.created_at
    const token = jwtSimple.encode(payload, secret);

    await sendResetPasswordEmail(user.firstName, `${user.id}/${token}`);
    res.json({ message: 'Please check your email to reset the password!' })

  } catch (error) {
    console.log(error.message);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.put('/password/reset', async (req, res) => {
  try {
    const { userId, token, password } = req.body;
    console.log(userId, token, password);

    const user = await knex("users").where("id", parseInt(userId)).first();
    if (!user) {
      res.status(400).json({ message: "Cannot reset password" });
      return;
    }
    console.log(user);
    const secret = user.password + "-" + user.created_at;

    const payload = jwtSimple.decode(token, secret);

    if (payload.userId !== user.id) {
      res.status(400).json({ message: "Cannot reset password" });
      return;
    }
    const newPassword = await hashPassword(password);

    await knex("users").update('password', newPassword).where("id", user.id);
    res.json({ message: 'Password updated successed' });
    return;
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ message: 'Cannot reset password' });
  }
})

app.post("/login/facebook", async (req, res) => {
  try {
    if (!req.body.accessToken) {
      res.status(401).json({ message: "Wrong Access Token!" });
      return;
    }
    const { accessToken } = req.body;
    const fetchResponse = await fetch(`https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`);
    const result = await fetchResponse.json();
    if (result.error) {
      res.status(401).json({ message: "Wrong Access Token!" });
      return;
    }
    let user = await knex("users").where("email", result.email).first();

    if (!user) {
      const lastName = result.name.split(" ").slice(-1)[0];
      const firstName = result.name.split(" ")[0];
      const email = result.email;
      const password = await hashPassword(Math.random().toString(36).slice(-10));
      const avatar = result.picture.data.url;

      let resUser = await knex("users").insert({ firstName, lastName, email, password, avatar }).returning("*");
      user = resUser[0];
    }
    const payload = {
      id: user.id,
    };
    const token = jwtSimple.encode(payload, jwt.jwtSecret);
    res.json({
      token: token
    });
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.post("/login", async (req, res) => {
  try {
    if (!req.body.email || !req.body.password) {
      res.status(401).json({ message: 'Missing Username/Password' });
      return;
    }
    const { email, password } = req.body;
    const user: User = await knex("users").where("email", email).first();

    if (!user || !(await checkPassword(password, user.password))) {
      res.status(401).json({ message: 'Invalid Username/Password' });
      return;
    }
    const payload = {
      id: user.id,
    };
    const token = jwtSimple.encode(payload, jwt.jwtSecret);
    res.json({ token });
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.use(isLoggedIn);

app.get("/user/info", async (req, res) => {
  try {
    res.json(req.user);
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ message: 'Internal server error' });
  }
});

const PORT = 8080;
app.listen(PORT, () => {
  console.log('listening on the PORT: ' + PORT);
})